# README #

Course notes for Udacity Intro to Web Design

### What is this repository for? ###

* Course notes and examples for [udacity](http://www.udacity.com/)'s [Intro to HTML](https://www.udacity.com/course/intro-to-html-and-css--ud304) course

### How do I get set up? ###

* One or more web browsers
* One or more plain text editors
* Specialized editor(s) for HTML
* Set up an account on [bitbucket](https://bitbucket.org)
* Learn how to use [git](https://git-scm.com/)